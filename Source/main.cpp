#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <filesystem>
#include <map>
#include <bitset>
#include <iterator>
#include <iomanip>
#include <cstdint>

using std::cout; using std::cerr;
using std::endl; using std::string;
using std::ifstream; using std::ostringstream;

// Read bytes from a file into a vector
std::vector<std::uint8_t> getBytes(std::ifstream& file, std::vector<std::uint8_t> temp, uintmax_t fileSize) {
	file.seekg(0);

	std::vector<std::uint8_t> data(fileSize);
	file.read(reinterpret_cast<char*>(data.data()), fileSize);

	data.resize(file.gcount());

	return data;
}

void countBytes(std::vector<std::uint8_t> &data, std::map<std::uint8_t, unsigned int> &byteCounts) {
	for (int i = 0; i < data.size(); i++) {
		if (byteCounts.find(data[i]) != byteCounts.end()) {
			byteCounts[data[i]]++;
		}
		else {
			byteCounts.emplace(data[i], 1);
		}
	}
	return;
}

void sortCounts(std::map<std::uint8_t, unsigned int>& byteCounts, std::multimap<unsigned int, std::uint8_t> &sortedCounts) {
	for (const auto& elem : byteCounts) {
		sortedCounts.emplace(elem.second, elem.first);
	}
	return;
}

void populateDictionary(std::multimap<unsigned int, std::uint8_t> &sortedCounts, std::map<std::uint8_t, string> &dictionary) {
	std::map<unsigned int, std::uint8_t>::reverse_iterator it;
	unsigned int i = 0;
	for (it = sortedCounts.rbegin(); it != sortedCounts.rend(); it++) {
		string value = "";
		for (int j = 0; j < i; j++) {
			value += "0";
		}
		value += "1";

		dictionary.emplace(it->second, value);
		i++;
	}
	return;
}

void compress(std::vector<std::uint8_t>& bytes, std::map<std::uint8_t, string>& dictionary, string& compressed) {
	for (int i = 0; i < bytes.size(); i++) {
		std::map<std::uint8_t, string>::iterator it = dictionary.find(bytes[i]);
		if (it != dictionary.end()) {
			compressed += it->second;
		}
	}
	return;
}

int main() {
	//string fileName = "test.txt";
	string fileName = "test.jpg";
	uintmax_t fileSize = std::filesystem::file_size(fileName);

	std::vector<std::uint8_t> bytes;
	std::map<std::uint8_t, unsigned int> byteCounts;
	std::multimap<unsigned int, std::uint8_t> sortedCounts;
	std::map<std::uint8_t, string> dictionary;
	string compressed = "";

	std::ifstream input(fileName, std::ios::in|std::ios::binary);
	bytes = getBytes(input, bytes, fileSize);
	countBytes(bytes, byteCounts);
	sortCounts(byteCounts, sortedCounts);
	populateDictionary(sortedCounts, dictionary);
	compress(bytes, dictionary, compressed);

	// Write compressed file to disk
	std::ofstream outFile;
	outFile.open("testOut.bin", std::ios::out | std::ios::binary);
	outFile << compressed;
	outFile.close();
	
	/*
	// Test writing bytes to a new file to ensure they were read correctly.
	std::ofstream outFile2;
	//outFile2.open("test3.txt", std::ios::out | std::ios::binary);
	outFile2.open("testOut.jpg", std::ios::out | std::ios::binary);
	for (unsigned int i = 0; i < bytes.size(); i++) {
		outFile2 << bytes[i];
	}
	outFile2.close();
	*/

	//for (const auto &elem : dictionary) {
	//	cout << elem.first << " : " << elem.second << endl;
	//}

	cout << compressed << endl;

	exit(0);
}